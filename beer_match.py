# Apprentice Coding Challenge
# TapHunter
# Author: Erin McDonald
# erinmcdonald@sandiego.edu

import csv
import difflib

# open csv files
with open('taphunter-beers.csv') as taphuntercsv:
    with open('ratebeer-beers.csv') as ratebeercsv:
        tapreader=csv.DictReader(taphuntercsv)
        ratebeercsv=csv.DictReader(ratebeercsv)

        #create dictionary with ratebeer names normalized and ratebeer ids
        ratenames={}      
        for raterow in ratebeercsv:
            ratenames[raterow['beer_common_name'].lower().strip()]=raterow['ratebeer_id']
        
        matches={}
        for taprow in tapreader:
           #commented out printlines in case you'd like to see them when you run it, was helpful during testing
           #print(taprow['beer_common_name'])

           #using difflib library to match beer names
           beermatch=difflib.get_close_matches(taprow['beer_common_name'].lower().strip(), ratenames.keys(),1)
           
           #print(beermatch)
           
           if beermatch:
               #dictionary entry taphunterid->ratebeerid
               matches[taprow['taphunter_id']]= ratenames[beermatch[0]]

        print(matches)
